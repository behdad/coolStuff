var app = angular.module("ProductApp",[]);
    app.controller("ctr",function($scope,$http){
        $http.get("data.php")
            .then(function(response) {
            $scope.myData = response.data.records;
                $scope.ProductItems = 11;
        });

        $scope.load = function(){
            $scope.ProductItems = $scope.ProductItems +4;
        }
    });

app.filter('customCurrency', function() {
    return function (price, symbol, place) {
        if (isNaN(price)) {
            return price;
        } else {
            var place = place === undefined ? true : place;
            if (place === true) {
                return symbol + price;
            } else {
                return price + symbol;
            }
        }
    }
})
